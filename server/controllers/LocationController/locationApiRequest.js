'use strict';

// Import the main utility file that will handle the processing logic for this
// controller file
const locationApiRequest = require('../../utils/locationApiRequest');

// Most of the surface level checkers should be contained w/in the controller
// file, so as to relieve the utility file of initial checking for variable
// existence and other easily checked user inputs
module.exports = (req, res, next) => {
  locationApiRequest({ query: req.query })
    .then(response => {
      req.responseData = {
        statusCode: 200,
        body: { data: response }
      };
      // Go to next middleware
      return next();
    })
    .catch(err => {
      req.responseData = {
        statusCode: err.statusCode || 404,
        body: { error: err.message || err }
      };
      // Go to next middleware
      return next();
    });
};
