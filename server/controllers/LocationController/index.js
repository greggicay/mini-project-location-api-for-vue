'use strict';

// Import the controller files here
const locationApiRequest = require('./locationApiRequest');

// Consolidate and export them as part of the `SampleController`
module.exports = {
  locationApiRequest
};
