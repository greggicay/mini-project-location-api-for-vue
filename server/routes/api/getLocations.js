'use strict';

const LocationController = require('../../controllers/LocationController');

/**
 * ** Adds sample api request route to the server **
 *
 * @param {*} router - is the express router object that we append our routes to
 *
 * @author Gregg Icay
 * @version 1.0.0
 */
module.exports = router => {
  router.get('/getLocations', LocationController.locationApiRequest);
};
