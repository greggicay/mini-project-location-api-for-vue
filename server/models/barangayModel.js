'use strict';

module.exports = (mongoose, Schema) => {
  const BarangaySchema = new Schema(
    {
      name: { type: String, required: true },
      parentId: { type: Schema.ObjectId, ref: 'Municipality', required: true }
    },
    { toJSON: { virtuals: true } }
  );

  return mongoose.model('Barangay', BarangaySchema);
};
