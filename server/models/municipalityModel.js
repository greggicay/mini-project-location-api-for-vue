'use strict';

module.exports = (mongoose, Schema) => {
  const MunicipalitySchema = new Schema(
    {
      name: { type: String, required: true },
      parentId: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'Province',
        required: true
      }
    },
    { toJSON: { virtuals: true } }
  );

  MunicipalitySchema.virtual('barangays', {
    ref: 'Barangay',
    localField: '_id',
    foreignField: 'parentId',
    justOne: false
  });

  return mongoose.model('Municipality', MunicipalitySchema);
};
