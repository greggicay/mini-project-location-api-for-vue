'use strict';

module.exports = (mongoose, Schema) => {
  const ProvinceSchema = new Schema(
    {
      name: { type: String, required: true }
    },
    { toJSON: { virtuals: true } }
  );

  ProvinceSchema.virtual('municipalities', {
    ref: 'Municipality',
    localField: '_id',
    foreignField: 'parentId',
    // Explicitly set justOne to false to always return array
    justOne: false
  });

  return mongoose.model('Province', ProvinceSchema);
};
