const ProvinceModel = require('../../models').ProvinceModel;
module.exports = () => {
  return new Promise((resolve, reject) => {
    ProvinceModel.find()
      .select('_id name')
      .lean()
      .exec((err, provinces) => {
        if (err || !provinces || !provinces.length) {
          // Throw error (to be catched by parent function).
          reject({
            statusCode: 404,
            message: err || 'Unable to get province details'
          });
        } else {
          // Format output as an array of objects with id and name
          const formattedProvinces = provinces.map(province => {
            return {
              id: province._id.toString(),
              name: province.name
            };
          });
          // Return formatted event reg fields
          resolve(formattedProvinces);
        }
      });
  });
};
