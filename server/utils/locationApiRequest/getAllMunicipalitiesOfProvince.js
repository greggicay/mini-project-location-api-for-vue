'use strict';

const ProvinceModel = require('../../models').ProvinceModel;
require('../../models').MunicipalityModel;
module.exports = ({ provinceName }) => {
  return new Promise((resolve, reject) => {
    ProvinceModel.findOne({ name: provinceName })
      .select('_id name')
      .populate({
        path: 'municipalities',
        select: '_id name'
      })
      .lean()
      .exec((err, province) => {
        if (err || !province || !province.municipalities) {
          // Throw error (to be catched by parent function).
          reject({
            statusCode: 404,
            message:
              err || `Unable to get municipality details of ${provinceName}`
          });
        } else {
          // Format output as an array of objects with id and name
          const formattedMunicipalities = province.municipalities.map(
            municipality => {
              return {
                id: municipality._id.toString(),
                name: municipality.name
              };
            }
          );
          // Return formatted event reg fields
          resolve(formattedMunicipalities);
        }
      });
  });
};
