'use strict';
const mongoose = require('mongoose');

const getAllProvinces = require('./getAllProvinces');
const getAllMunicipalitiesOfProvince = require('./getAllMunicipalitiesOfProvince');
const getAllBarangaysOfMunicipality = require('./getAllBarangaysOfMunicipality');
// Do all your processing logic here, or within the utility files. The main
// utility file, index.js, should give you an overview of what the flow of the
// processing logic is
module.exports = async ({ query }) => {
  let response = { message: 'No locations found.' };
  if (Object.keys(query).length === 0) {
    response = await getAllProvinces();
  } else if (query.province && !query.municipality) {
    response = await getAllMunicipalitiesOfProvince({
      provinceName: query.province
    });
  } else if (query.province && query.municipality) {
    response = await getAllBarangaysOfMunicipality({
      provinceName: query.province,
      municipalityName: query.municipality
    });
  }

  return {
    response
  };
};
