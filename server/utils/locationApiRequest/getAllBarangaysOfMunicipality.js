'use strict';

const ProvinceModel = require('../../models').ProvinceModel;
require('../../models').MunicipalityModel;
require('../../models').BarangayModel;
module.exports = ({ provinceName, municipalityName }) => {
  return new Promise((resolve, reject) => {
    ProvinceModel.findOne({ name: provinceName })
      .select('_id name')
      .populate({
        path: 'municipalities',
        select: '_id name',
        match: { name: municipalityName },
        populate: {
          path: 'barangays',
          select: '_id name'
        }
      })
      .lean()
      .exec((err, province) => {
        if (
          err ||
          !province ||
          !province.municipalities ||
          !province.municipalities.length ||
          !province.municipalities[0] ||
          !province.municipalities[0].barangays ||
          !province.municipalities[0].barangays.length
        ) {
          // Throw error (to be catched by parent function).
          reject({
            statusCode: 404,
            message:
              err ||
              `Unable to get barangay details of ${municipalityName}, ${provinceName}`
          });
        } else {
          // Format output as an array of objects with id and name
          const formattedBarangays = province.municipalities[0].barangays.map(
            barangay => {
              return {
                id: barangay._id.toString(),
                name: barangay.name
              };
            }
          );
          // Return formatted event reg fields
          resolve(formattedBarangays);
        }
      });
  });
};
